if ! test -d $sugar_install_directory/config/prompt
  mkdir -p $sugar_install_directory/config/prompt
end

if ! test -f $sugar_install_directory/config/prompt/00-last-status.fish
  cp $argv[1]/prompt/00-last-status.fish $sugar_install_directory/config/prompt
end

if ! test -f $sugar_install_directory/config/prompt/50-primary.fish
  cp $argv[1]/prompt/50-primary.fish $sugar_install_directory/config/prompt
end

function fish_prompt
  for file in $sugar_install_directory/config/prompt/*.fish
    source $file
  end
end
