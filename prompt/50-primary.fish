if test $last_status -eq 0
  set_color green
  printf "■ "
  set_color normal
else
  set_color red
  printf "■ %d " $last_status
  set_color normal
end

if test $USER = "root"
  set_color -o red
  printf "%s" (whoami)
else
  set_color -o
  printf "%s" (whoami)
end

set_color -o green
printf "@"
set_color -o magenta
printf "%s " (hostname -f)
set_color normal

set_color -o cyan
printf "%s " (prompt_pwd)
set_color normal
